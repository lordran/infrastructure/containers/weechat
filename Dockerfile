FROM debian:bullseye-slim

RUN apt-get update && \
    apt-get install -y weechat weechat-headless weechat-curses weechat-scripts weechat-lua

RUN useradd -s /bin/bash -m weechat

USER weechat
WORKDIR /home/weechat

ENTRYPOINT [ "weechat-headless" ]
